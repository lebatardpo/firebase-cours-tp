# Projet Android TP du cours Firebase 

## SHA-1
N'oubliez pas d'ajouter l'empreinte SHA-1 de votre ordi pour pouvoir accéder à Firebase
```bash
$ keytool -exportcert -list -v -alias androiddebugkey -keystore ~/.android/debug.keystore
# <Entré> quand il demande le mot de passe
```
## google-services.json
N'oubliez pas d'ajouter le fichier `google-services.json` dans le dossier `app/` pour accéder à Firebase

### _Par Alex, Erwan, Julien_
