package eisti.firebasetp.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import eisti.firebasetp.R;

public class ProfileImageFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = "ProfileImageFragment";
    private static final int PICK_IMAGE = 42;

    private Activity activity;

    private ImageView imgProfilePic;
    private Button btnProfilePic;

    public static ProfileImageFragment newInstance() {
        ProfileImageFragment fragment = new ProfileImageFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profileimage, container, false);

        this.activity = getActivity();

        this.imgProfilePic = view.findViewById(R.id.imgProfilePic);
        this.btnProfilePic = view.findViewById(R.id.btnUploadProfilePic);
        this.btnProfilePic.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == btnProfilePic.getId()) {
            chooseImage();
        }
    }

    public void chooseImage() {
        // DELETE START
        // Ask user to pick an image
        // DELETE END
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE) {
            // DELETE START
            // Upload image in Firebase Storage
            // DELETE END
        }
    }

    private void loadImage() {
        // DELETE START
        // DOWNLOAD IMAGE
        // DELETE END
    }
}
