package eisti.firebasetp.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;

import eisti.firebasetp.R;
import eisti.firebasetp.fragment.ProfileImageFragment;
import eisti.firebasetp.fragment.ProfileInfosFragment;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            finish();
            return;
        }

        getFragmentManager().beginTransaction()
                .add(R.id.profileInfosFragment, ProfileInfosFragment.newInstance())
                .add(R.id.profilePicFragment, ProfileImageFragment.newInstance())
                .commit();
    }
}
