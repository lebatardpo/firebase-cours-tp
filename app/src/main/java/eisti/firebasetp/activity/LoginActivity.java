package eisti.firebasetp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;

import eisti.firebasetp.R;
import eisti.firebasetp.fragment.EmailSigninFragment;
import eisti.firebasetp.fragment.GoogleSigninFragment;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            startActivity(new Intent(this, ProfileActivity.class));
            return;
        }

        getFragmentManager().beginTransaction()
                .add(R.id.emailSignInFragment, EmailSigninFragment.newInstance())
                .add(R.id.googleSignInFragment, GoogleSigninFragment.newInstance())
                .commit();
    }
}
